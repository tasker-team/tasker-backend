package com.braincrysis.tasker.controller;

import com.braincrysis.tasker.domain.TaskEntity;
import com.braincrysis.tasker.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * @author mtolstyh
 * @since 24.01.2017.
 */

@RestController
@RequestMapping("/rest/api/0.1")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @ResponseBody
    @RequestMapping(value = "{entity}", method = GET)
    public Page<TaskEntity> getList(@PathVariable("entity") String entity,
                                    @RequestParam("page") int page,
                                    @RequestParam("size") int size) {
        return taskService.get(entity, page, size);
    }

    @ResponseBody
    @RequestMapping(value = "{entity}/{id}", method = GET)
    public ResponseEntity get(@PathVariable("entity") String entity,
                              @PathVariable("id") String id) {
        return ResponseEntity.ok(taskService.get(entity, id));
    }

    @ResponseBody
    @RequestMapping(value = "{entity}", method = POST)
    public ResponseEntity<TaskEntity> post(@PathVariable("entity") String entity,
                                           @RequestBody TaskEntity object) {
        return ResponseEntity.ok(taskService.save(entity, object));
    }

    @ResponseBody
    @RequestMapping(value = "{entity}/bulk", method = POST)
    public ResponseEntity<TaskEntity> postBulk(@PathVariable("entity") String entity,
                                           @RequestBody TaskEntity object) {
        return ResponseEntity.ok(taskService.saveBulk(entity, object));
    }

    @ResponseBody
    @RequestMapping(value = "{entity}/{id}", method = PUT)
    public ResponseEntity<TaskEntity> put(@PathVariable("entity") String entity,
                                          @PathVariable("id") String id, //
                                          @RequestBody TaskEntity object) {
        return ResponseEntity.ok(taskService.update(entity, object));
    }

    @ResponseBody
    @RequestMapping(value = "{entity}/{id}", method = DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable("entity") String entity,
                                          @PathVariable("id") String id) {
        taskService.delete(entity, id);
        return ResponseEntity.ok(Boolean.TRUE);
    }
}
