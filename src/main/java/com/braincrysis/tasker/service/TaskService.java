package com.braincrysis.tasker.service;

import com.braincrysis.tasker.domain.Task;
import com.braincrysis.tasker.domain.TaskEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mtolstyh
 * @since 24.01.2017.
 */
@Service
public class TaskService {

    @Autowired
    private MongoTemplate mongoTemplate;

    ObjectMapper objectMapper = new ObjectMapper();

    public Page<TaskEntity> get(String entity, int page, int size) {
        Query query = new Query();
        query.skip(page * size).limit(size);
        return new PageImpl<>(
                mongoTemplate.find(query, TaskEntity.class, entity),
                new PageRequest(page, size),
                mongoTemplate.count(new Query(), entity));
    }

    public TaskEntity get(String entity, String id) {
        return mongoTemplate.findOne(Query.query(Criteria.where("id").is(id)), TaskEntity.class, entity);
    }

    public TaskEntity save(String entity, TaskEntity document) {
        mongoTemplate.save(document, entity);
        return document;
    }

    public TaskEntity saveBulk(String entity, TaskEntity list) {
        try {
            List<Object> objectList = (List<Object>) list.data;

            List<Object> inserted = new ArrayList<>();
            List<Object> merged = new ArrayList<>();

            objectList.forEach(task -> {
                String taskAsString = null;
                try {
                    taskAsString = objectMapper.writeValueAsString(task);
                    Task t = objectMapper.readValue(taskAsString, Task.class);

                    List<TaskEntity> finded = mongoTemplate.find(
                            Query.query(Criteria.where("data.date").is(t.date.getTime())),
                            TaskEntity.class,
                            entity
                    );
                    finded.forEach(tsk -> {
                        tsk.data = t;
                        mongoTemplate.save(tsk, entity);
                        merged.add(t);
                    });

                    if (!merged.contains(t)) {
                        TaskEntity taskToSave = new TaskEntity(null, t);
                        mongoTemplate.save(taskToSave, entity);
                        inserted.add(taskToSave);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (ClassCastException e) {
            return null;
        }
        return list;
    }

    public TaskEntity update(String entity, TaskEntity document) {
        mongoTemplate.save(document, entity);
        return document;
    }

    public void delete(String entity, String id) {
        mongoTemplate.remove(Query.query(Criteria.where("id").is(id)), entity);
    }
}
