package com.braincrysis.tasker.service;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author mtolstyh
 * @since 09.09.2016.
 */
@Component
final public class LongToDateConverter implements Converter<Long, Date> {
    @Override
    public Date convert(Long source) {
        return new Date(source);
    }
}