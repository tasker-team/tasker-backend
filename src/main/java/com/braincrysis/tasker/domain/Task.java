package com.braincrysis.tasker.domain;

import java.util.Date;

/**
 * @author mtolstyh
 * @since 19.02.2016.
 */
public class Task {
    public String number;
    public String task;
    public String title;
    public Date date;
    public Date end;
    public Boolean status; //0 complete | 1 progress
}
