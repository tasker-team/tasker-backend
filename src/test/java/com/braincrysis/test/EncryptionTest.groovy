package com.braincrysis.test

import com.braincrysis.tasker.domain.Task
import com.braincrysis.tasker.service.Encryptor
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Specification

class EncryptionTest extends Specification {

    @Autowired
    Encryptor encryptor

    def "test simple task encription" () {
        given:
            def task = new Task(
                    number: '1',
                    task: "Sample Task",
                    title: "Encrypt",
                    date: new Date()
            )

        when:
            encryptor.process(task)
    }

}